# Faire à la place de :

## Exemple de départ :

A - Coucou Arnaud, j'ai fais une modif sur le fichier tex de l'aide infligée, tu pourrais m'expliquer comment faire pour commit et push ?
B - Attends, je vais te faire ça, ça me prend une minute.
A - Ok, merci.

A - Ah zut, j'avais mis un "s" de trop dans un slide. J'ai fais la modif, tu peux ....

## Super Arnaud :

A - Coucou Arnaud, j'ai fais une modif sur le fichier tex de l'aide infligée, tu pourrais m'expliquer comment faire pour commit et push ?
B - Je ne suis pas certain de comprendre ce que tu veux. Tu souhaites que ce soit mis à jour ou apprendre à le faire ?
A - C'est la même chose non ?
B - Disons que dans un cas je peux juste le faire et ce sera mis à jour. Dans le deuxième cas je peux prendre du temps avec toi et t'apprendre à le faire.
A - Ah oui... là je n'ai pas très envie d'apprendre à faire ça. Tu peux le faire ?

## Super Ginette :

A - Coucou Arnaud, j'ai fais une modif sur le fichier tex de l'aide infligée, tu pourrais m'expliquer comment faire pour commit et push ?
B - Attends, je vais te faire ça, ça me prend une minute.
A - Merci, j'apprécie l'intention. Par contre ma demande serait d'apprendre à le faire. Est-ce que tu pourrais m'apprendre.
B - Pas de soucis, on se fait ça après la rétro ?

# Envie d'en faire trop :

## Exemple de départ :

A - Hello, tiens tu veux bien regarder ma prés sur l'aide infligée et me corriger les fautes d'orthographe ?
B - Ah tiens à ta place j'aurais mis des exemples.
A - Oui, oui c'est prévu, mais je ne les écris pas dans la prés.
B - Ah et tu as changé cette image là ?
A - Oui, finalement l'autre était vraiment de trop mauvaise qualité.
B - Tu sais que tu peux aussi utiliser d'autres logiciels pour faire ça ?
A - Oui, oui, je sais, mais j'ai un collègue qui aime beaucoup Latex !!!
B - Ah et là aussi j'aurai mis ... Et puis ... Ici aussi....

## Super Arnaud :

A - Hello, tiens tu veux bien regarder ma prés sur l'aide infligée et me corriger les fautes d'orthographe ?
B - Ah tiens à ta place j'aurais mis des exemples.
A - Oui, mais je ne les écris pas dans la prés.
B - Il me semble que je ne comprends pas bien ce que tu me demandes. Tu veux que je te donnes des conseils sur ta prés ?
A - En fait là, j'aimerais juste que tu me corriges les fautes. Tu vois les gens attendent, c'est pas vraiment le moment de rediscuter de la structure.
B - Ok, bon tu vois là..... ah et là pourquoi est-ce que tu as choisis cette image ?
A - ... :-/
B - Toujours pas le moment de discuter de ça n'est-ce pas ?
A - C'est ça :-D

## Super Ginette

A - Hello, tiens tu veux bien regarder ma prés sur l'aide infligée et me corriger les fautes d'orthographe ?
B - Ah tiens à ta place j'aurais mis des exemples.
A - Je serais heureuse de t'expliquer plus en détails pourquoi j'ai fais les choses. Par contre juste là maintenant j'aimerais que tu m'aides à regarder juste l'orthographe, c'est ok pour toi ?
B - Ah oui bien évidemment. Tiens je vais me mettre sur ton ordi et on va corriger ça ensemble.

# Tu as besoin de la même chose que moi

## Exemple de départ

A - J'ai des soucis sur ce projet, je ne sais pas comment m'y prendre.
B - C'est simple, rajoute 3 personnes et vous irez deux fois plus vite.
A - Je suis convaincue que nous pouvons faire avec l'équipe actuelle, mais je ne trouve pas encore comment.
B - Mais nan je te dis, tu te poses beaucoup trop de questions, rajoute du monde et ça ira tout seul.

Mais aussi :

A - J'ai des soucis sur ce projet, je ne sais pas comment m'y prendre.
B - C'est simple, rajoute 3 personnes et vous irez deux fois plus vite.
A - Je suis convaincue que nous pouvons faire avec l'équipe actuelle, mais je ne trouve pas encore comment.
B - Nan mais si tu ne veux pas de mon aide, ne me demande pas !!

## Super Arnaud

A - J'ai des soucis sur ce projet, je ne sais pas comment m'y prendre.
B - C'est simple, rajoute 3 personnes et vous irez deux fois plus vite.
A - Je suis convaincue que nous pouvons faire avec l'équipe actuelle, mais je ne trouve pas encore comment.
B - Je pense que je ne comprends pas très bien ton problème. Peux-tu me dire comment je peux t'apporter de l'aide ?

## Super Ginette

A - J'ai des soucis sur ce projet, je ne sais pas comment m'y prendre.
B - C'est simple, rajoute 3 personnes et vous irez deux fois plus vite.
A - Merci beaucoup pour tes idées. J'aimerais le faire différemment. Est-ce que tu accepterais que je te présente ma problématique et mes objectifs plus en détails pour quoi tu puisses me donner ton avis.

# Je vais te sauver

## Exemple de départ :

A - J'ai entendu dire que tu reprenais le poste de Bernard ?
B - Oui, je suis content-e.
A - Ah oui c'est super, mais tu dois être stressé-e. La personne avant toi est partie au bout de 6 mois tellement c'était difficile.
B - Ca va, je me dis que j'ai une bonne équipe.
A - Tu as bien du courage ! Moi à ta place je paniquerais. Bon mais de toutes façons je vais t'envoyer des documents pour que tu puisses apprendre comment faire.
    Ne t'inquiète pas, on va tous te soutenir dans cette épreuve. Et surtout surtout ... ne me dit pas merci, c'est normal.
    
## Super Arnaud :

A - J'ai entendu dire que tu reprenais le poste de Bernard ?
B - Oui, je suis content-e.
A - Bon je vois que je suis inquiet pour toi, alors que toi tu as l'air de gérer parfaitement. Du coup je tenais juste à te dire félicitation et n'héiste pas à me dire si tu as besoin de qqch.

## Super Ginette :

A - J'ai entendu dire que tu reprenais le poste de Bernard ?
B - Oui, je suis content-e.
A - Ah oui c'est super, mais tu dois être stressé-e. La personne avant toi est partie au bout de 6 mois tellement c'était difficile. Et ....
B - Dis donc Arnaud, j'ai l'impression que tu es stressé pour moi. Je suis heureuse de voir que tu t'inquiète de mon bien être. Pour l'instant tout va bien, est-ce que tu voudrais pas qu'on fête cette bonne nouvelle ensemble ;-)

